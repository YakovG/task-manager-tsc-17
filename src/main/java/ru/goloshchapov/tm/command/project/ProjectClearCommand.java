package ru.goloshchapov.tm.command.project;

import ru.goloshchapov.tm.exception.entity.ProjectNotFoundException;
import ru.goloshchapov.tm.model.Project;

import java.util.List;

public final class ProjectClearCommand extends AbstractProjectCommand{

    public static final String NAME = "project-clear";

    public static final String DESCRIPTION = "Clear all projects";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT CLEAR]");
        List<Project> projects = serviceLocator.getProjectTaskService().findAllProjects();
        if (projects == null) throw new ProjectNotFoundException();
        for (final Project project: projects) {
            serviceLocator.getProjectTaskService().removeAllByProjectId(project.getId());
        }
        projects.clear();
        serviceLocator.getProjectTaskService().clearProjects();
    }
}
