package ru.goloshchapov.tm.command.project;

import ru.goloshchapov.tm.enumerated.Sort;
import ru.goloshchapov.tm.model.Project;
import ru.goloshchapov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListSortCommand extends AbstractProjectCommand{

    public static final String NAME = "project-sorted-list";

    public static final String DESCRIPTION = "Show sorted project list";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT SORTED LIST]");
        System.out.println("ENTER SORT TYPE");
        System.out.println(Arrays.toString(Sort.values()));
        System.out.println("DEFAULT TYPE = CREATED");
        final String sortType = TerminalUtil.nextLine();
        final List<Project> projects = serviceLocator.getProjectService().sortedBy(sortType);
        int index = 1;
        for (final Project project: projects) {
            System.out.println(index + ". " + project);
            index++;
        }
    }
}
