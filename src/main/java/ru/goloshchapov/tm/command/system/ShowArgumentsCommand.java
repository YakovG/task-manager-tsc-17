package ru.goloshchapov.tm.command.system;

import ru.goloshchapov.tm.command.AbstractCommand;

import java.util.Collection;

public final class ShowArgumentsCommand extends AbstractCommand {

    public static final String ARGUMENT = "-arg";

    public static final String NAME = "arguments";;

    public static final String DESCRIPTION = "Show program arguments";

    @Override
    public String arg() {
        return ARGUMENT;
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final Collection<String> arguments= serviceLocator.getCommandService().getListCommandArgs();
        for (final String arg: arguments) System.out.println(arg);
    }
}