package ru.goloshchapov.tm.command.system;

import ru.goloshchapov.tm.command.AbstractCommand;

public final class VersionCommand extends AbstractCommand {

    public static final String ARGUMENT = "-v";

    public static final String NAME = "version";

    public static final String DESCRIPTION = "Show application version";

    @Override
    public String arg() {
        return ARGUMENT;
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }
}
