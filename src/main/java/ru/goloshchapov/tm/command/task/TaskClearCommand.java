package ru.goloshchapov.tm.command.task;

public final class TaskClearCommand extends AbstractTaskCommand{

    public static final String NAME = "task-clear";

    public static final String DESCRIPTION = "Clear all tasks";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[TASK CLEAR]");
        serviceLocator.getTaskService().clear();
    }
}
