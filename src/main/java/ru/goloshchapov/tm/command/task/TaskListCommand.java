package ru.goloshchapov.tm.command.task;

import ru.goloshchapov.tm.model.Task;

import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand {

    public static final String NAME = "task-list";

    public static final String DESCRIPTION = "Show task list";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");
        final List<Task> tasks = serviceLocator.getTaskService().findAll();
        int index = 1;
        for (final Task task: tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
    }
}
