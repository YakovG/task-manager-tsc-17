package ru.goloshchapov.tm.command.task;

import ru.goloshchapov.tm.exception.entity.TaskNotFoundException;
import ru.goloshchapov.tm.model.Task;
import ru.goloshchapov.tm.util.TerminalUtil;

public final class TaskByProjectIdUnbindCommand extends AbstractTaskCommand{

    public static final String NAME = "task-unbind-from-project-by-id";

    public static final String DESCRIPTION = "Remove task from project by id";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("REMOVE TASK FROM PROJECT");
        System.out.println("ENTER TASK ID");
        final String taskId = TerminalUtil.nextLine();
        System.out.println("ENTER PROJECT ID");
        final String projectId = TerminalUtil.nextLine();
        final Task task = serviceLocator.getProjectTaskService().unbindFromProjectById(projectId, taskId);
        if (task == null) throw new TaskNotFoundException();
    }
}
