package ru.goloshchapov.tm.exception.system;

import ru.goloshchapov.tm.exception.AbstractException;

public class IndexIncorrectException extends AbstractException {

    public IndexIncorrectException() {
        super("Error! Index is incorrect...");
    }

    public IndexIncorrectException(String value) {
        super("Error! This value " + value + " is not a number...");
    }

}
