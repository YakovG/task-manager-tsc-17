package ru.goloshchapov.tm.exception.incorrect;

import ru.goloshchapov.tm.exception.AbstractException;

public class StatusIncorrectException extends AbstractException {

    public StatusIncorrectException() {
        super("Error! Status is incorrect...");
    }

}
