package ru.goloshchapov.tm.util;

public interface ValidationUtil {

    static boolean isEmpty(final String value) {
        return value == null || value.isEmpty();
    }

    static boolean checkIndex(final int index, final int size) {
        if (index < 0) return false;
        return index < size;
    }

    static boolean checkInclude(final String elem, String[] set) {
        boolean check = false;
        for (String step: set) {
            check = elem.equals(step);
            if (check) break;
        }
        return check;
    }

}
