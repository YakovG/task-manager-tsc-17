package ru.goloshchapov.tm.entity;

import ru.goloshchapov.tm.enumerated.Status;

public interface IHaveStatus {

    Status getStatus();

    void setStatus(Status status);

}
