package ru.goloshchapov.tm.api.service;

public interface ServiceLocator {

    ICommandService getCommandService();

    ITaskService getTaskService();

    IProjectService getProjectService();

    IProjectTaskService getProjectTaskService();

}
