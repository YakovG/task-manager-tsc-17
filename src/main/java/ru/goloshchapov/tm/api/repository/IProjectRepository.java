package ru.goloshchapov.tm.api.repository;

import ru.goloshchapov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository {

    List<Project> findAll();

    List<Project> findAll(Comparator<Project> comparator);

    List<Project> findAllStarted(Comparator<Project> comparator);

    List<Project> findAllCompleted(Comparator<Project> comparator);

    int size();

    void add(Project project);

    void remove(Project project);

    void clear();

    Project findOneById(String id);

    Project removeOneById(String id);

    Project findOneByIndex(Integer index);

    Project findOneByName(String name);

    boolean isAbsentById(String id);

    boolean isAbsentByIndex(Integer index);

    boolean isAbsentByName(String name);

    String getIdByName(String name);

    String getIdByIndex(Integer index);

    Project removeOneByIndex(Integer index);

    Project removeOneByName(String name);

    Project startProjectById(String id);

    Project startProjectByIndex(Integer index);

    Project startProjectByName(String name);

    Project finishProjectById(String id);

    Project finishProjectByIndex(Integer index);

    Project finishProjectByName(String name);

}
