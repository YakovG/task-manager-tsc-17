package ru.goloshchapov.tm.service;

import ru.goloshchapov.tm.api.repository.ICommandRepository;
import ru.goloshchapov.tm.api.service.ICommandService;
import ru.goloshchapov.tm.command.AbstractCommand;

import java.util.Collection;

import static ru.goloshchapov.tm.util.ValidationUtil.isEmpty;

public final class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Collection<AbstractCommand> getCommands() {return commandRepository.getCommands();}

    @Override
    public Collection<AbstractCommand> getArguments() { return commandRepository.getArguments();}

    @Override
    public Collection<String> getListCommandNames() {
        return commandRepository.getCommandNames();
    }

    @Override
    public Collection<String> getListCommandArgs() {
        return commandRepository.getCommandArgs();
    }

    @Override
    public AbstractCommand getCommandByName(String name) {
        if (isEmpty(name)) return null;
        return commandRepository.getCommandByName(name);
    }

    @Override
    public AbstractCommand getCommandByArg(String arg) {
        if (isEmpty(arg)) return null;
        return commandRepository.getCommandByArg(arg);
    }

    @Override
    public void add(AbstractCommand command) {
        if (command == null) return;
        commandRepository.add(command);
    }

}
