package ru.goloshchapov.tm.service;

import ru.goloshchapov.tm.api.service.ILoggerService;

import java.io.IOException;
import java.util.logging.*;

import static ru.goloshchapov.tm.util.ValidationUtil.isEmpty;

public class LoggerService implements ILoggerService {

    private static final String COMMANDS = "COMMANDS";
    private static final String COMMANDS_FILE = "./access.log";

    private static final String ERRORS = "ERRORS";
    private static final String ERRORS_FILE = "./errors.log";

    private static final String MESSAGES = "MESSAGES";
    private static final String MESSAGES_FILE = "./messages.txt";

    private final LogManager manager = LogManager.getLogManager();
    private final Logger root = Logger.getLogger("");
    private final Logger commands = Logger.getLogger(COMMANDS);
    private final Logger errors = Logger.getLogger(ERRORS);
    private final Logger messages = Logger.getLogger(MESSAGES);

    {
        init();
        registry(commands, COMMANDS_FILE, false);
        registry(errors, ERRORS_FILE, true);
        registry(messages, MESSAGES_FILE, true);
    }

    private ConsoleHandler getConsoleHandler() {
        final ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new Formatter() {
            @Override
            public String format(LogRecord record) {
                return record.getMessage() + "\n";
            }
        });
        return handler;
    }

    private void init() {
        try {
            manager.readConfiguration(LoggerService.class.getResourceAsStream("/logger.properties"));
        } catch (IOException e) {
            root.severe(e.getMessage());
        }
    }

    private void registry (
            final Logger logger, final String filename, final boolean isConsole) {
        try {
            if (isConsole) logger.addHandler(getConsoleHandler());
            logger.setUseParentHandlers(false);
            logger.addHandler(new FileHandler(filename));
        } catch (final IOException e) {
            root.severe(e.getMessage());
        }
    }

    @Override
    public void info(final String message) {
        if (isEmpty(message)) return;
        messages.info(message);
    }

    @Override
    public void debug(final String message) {
        if (isEmpty(message)) return;
        messages.fine(message);
    }

    @Override
    public void command(final String message) {
        if (isEmpty(message)) return;
        commands.info(message);
    }

    @Override
    public void error(final Exception e) {
        if (e == null) return;
        errors.log(Level.SEVERE, e.getMessage(), e);
    }

}
