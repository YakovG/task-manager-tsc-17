package ru.goloshchapov.tm.bootstrap;

import ru.goloshchapov.tm.api.*;
import ru.goloshchapov.tm.api.repository.ICommandRepository;
import ru.goloshchapov.tm.api.repository.IProjectRepository;
import ru.goloshchapov.tm.api.repository.ITaskRepository;
import ru.goloshchapov.tm.api.service.*;
import ru.goloshchapov.tm.command.AbstractCommand;
import ru.goloshchapov.tm.command.project.*;
import ru.goloshchapov.tm.command.system.*;
import ru.goloshchapov.tm.command.task.*;
import ru.goloshchapov.tm.exception.system.UnknownArgumentException;
import ru.goloshchapov.tm.exception.system.UnknownCommandException;
import ru.goloshchapov.tm.repository.CommandRepository;
import ru.goloshchapov.tm.repository.ProjectRepository;
import ru.goloshchapov.tm.repository.TaskRepository;
import ru.goloshchapov.tm.service.*;
import ru.goloshchapov.tm.util.TerminalUtil;

import static ru.goloshchapov.tm.util.ValidationUtil.isEmpty;

public class Bootstrap implements ServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    private final ILoggerService loggerService = new LoggerService();

    {
        registry(new AboutCommand());
        registry(new VersionCommand());
        registry(new HelpCommand());
        registry(new SystemInfoCommand());
        registry(new ShowCommandsCommand());
        registry(new ShowArgumentsCommand());
        registry(new ExitCommand());
        registry(new TaskCreateCommand());
        registry(new TaskListCommand());
        registry(new TaskListSortCommand());
        registry(new TaskClearCommand());
        registry(new TaskByIdViewCommand());
        registry(new TaskByIndexViewCommand());
        registry(new TaskByNameViewCommand());
        registry(new TaskByIdRemoveCommand());
        registry(new TaskByIndexRemoveCommand());
        registry(new TaskByNameRemoveCommand());
        registry(new TaskByIdUpdateCommand());
        registry(new TaskByIndexUpdateCommand());
        registry(new TaskByIdStartCommand());
        registry(new TaskByIndexStartCommand());
        registry(new TaskByNameStartCommand());
        registry(new TaskByIdFinishCommand());
        registry(new TaskByIndexFinishCommand());
        registry(new TaskByNameFinishCommand());
        registry(new TaskByIdChangeStatusCommand());
        registry(new TaskByIndexChangeStatusCommand());
        registry(new TaskByNameChangeStatusCommand());
        registry(new TaskByProjectIdListCommand());
        registry(new TaskByProjectIndexListCommand());
        registry(new TaskByProjectNameListCommand());
        registry(new TaskByProjectIdBindCommand());
        registry(new TaskByProjectIdUnbindCommand());
        registry(new TaskByProjectIdRemoveAllCommand());
        registry(new TaskByProjectIndexRemoveAllCommand());
        registry(new TaskByProjectNameRemoveAllCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectListCommand());
        registry(new ProjectListSortCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectByIdViewCommand());
        registry(new ProjectByIndexViewCommand());
        registry(new ProjectByNameViewCommand());
        registry(new ProjectByIdRemoveCommand());
        registry(new ProjectByIndexRemoveCommand());
        registry(new ProjectByNameRemoveCommand());
        registry(new ProjectByIdUpdateCommand());
        registry(new ProjectByIndexUpdateCommand());
        registry(new ProjectByIdStartCommand());
        registry(new ProjectByIndexStartCommand());
        registry(new ProjectByNameStartCommand());
        registry(new ProjectByIdFinishCommand());
        registry(new ProjectByIndexFinishCommand());
        registry(new ProjectByNameFinishCommand());
        registry(new ProjectByIdChangeStatusCommand());
        registry(new ProjectByIndexChangeStatusCommand());
        registry(new ProjectByNameChangeStatusCommand());
        registry(new TestDataCreateCommand());
    }

    private void registry(final AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void parseArg(final String arg) {
        if (isEmpty(arg)) return;
        final AbstractCommand command = commandService.getCommandByArg(arg);
        if (command == null) throw new UnknownArgumentException(arg);
        command.execute();
    }

    private void parseCommand(final String cmd) {
        if (isEmpty(cmd)) return;
        final AbstractCommand command = commandService.getCommandByName(cmd);
        if (command == null) throw new UnknownCommandException(cmd);
        command.execute();
    }

    private boolean parseArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        try {
            parseArg(arg);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.err.println("[FAIL]");
        }
        return true;
    }

    public void run(String... args) {
        loggerService.debug("DEBUG INFO!");
        loggerService.info("*** WELCOME TO TASK MANAGER ***");
        if (parseArgs(args)) System.exit(0);
        System.out.println("USE TEST DATASET? (y)");
        if ("y".equals(TerminalUtil.nextLine())) {
            parseCommand("create-test-data");
            loggerService.info("TEST DATASET CREATED");
        }
        System.out.println("ENTER COMMAND:");
        while (true) {
            final String cmd = TerminalUtil.nextLine();
            loggerService.command(cmd);
            try {
                parseCommand(cmd);
                System.out.println("[OK]");
            } catch (Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
                System.err.println("[ENTER NEW COMMAND]");
            }
        }
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() { return projectTaskService; }
}
